# 影视站小程序——对接MAC CMS版

## 起因

学了小程序之后，想着小程序还挺棒的的，然后再微信搜索了一下几个影视小程序。无一例外，都是垃圾。广告多还不算，还不能播放。。遂开始自己写。其实开始写在很久之前就开始了，不过被各种各样的事情耽搁了。一直拖到现在，还是个半成品。

## 功能介绍

对接MACCMS影视站程序。简单添加几行代码就可以直接同步更新（主要还是我懒得写后台程序）

支持M3U8、MP4等格式视频（不是我规定的，微信官方定的-。-）

支持多源替换，在存在多家符合条件的资源站时，显示全部。

支持分类显示、搜索、播放、详情介绍、上/下一集。

好像就没了。。

## 后续准备添加的功能：

##### 个人中心

历史播放记录、收藏夹

##### 分类

细分分类，按年份、视频分类、国家、热度、评分等进行排序显示

##### 播放页

增加随机推荐

##### 其他

可能会加点放广告的地方。不过我这个人是真滴讨厌广告。自己的小程序是肯定不会加的。

## 最后

本小程序尚未完善，仅是demo版本。视频资源全部存储于网络，本小程序并不存储任何数据，不承担任何责任。最后吹一下MACCMS 看了代码，作者想的是真滴周到！点赞！