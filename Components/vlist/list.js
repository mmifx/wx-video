// Components/vlist/list.js
Component({
  options: {
    styleIsolation: 'isolated'
  },
  /**
   * 组件的属性列表
   */
  properties: {
    url: String,
    iconname: String,
    title: String,
    isTopType: Boolean,
    typeid: Number
  },

  /**
   * 组件的初始数据
   */
  data: {
    videoList: [{
      vod_pic: getApp().globalData.logosrc
    }]
  },

  methods: {
    toplay: function (e) {
      wx.navigateTo({
        url: '/pages/play/play?id=' + e.currentTarget.dataset.id,
      })
    },
    golist: function (e) {
      wx.navigateTo({
        url: '/pages/list/list?id=' + e.currentTarget.dataset.id,
      })
    },
    _getVideoList() {
      let that = this;
      if (that.data.isTopType) {
        wx.request({
          method: 'GET',
          url: this.data.url + 'api.php/provide/vod/',
          data: {
            ac: 'detail',
            tt: that.data.typeid,
            h: "720"
          },
          success(res) {
            that.setData({
              videoList: res.data.list
            })
          }
        })
      } else {
        wx.request({
          method: 'GET',
          url: this.data.url + 'api.php/provide/vod/',
          data: {
            ac: 'detail',
            t: that.data.typeid,
            h: "720"
          },
          success(res) {
            that.setData({
              videoList: res.data.list
            })
          }
        })
      }
    }
  },
  lifetimes: {
    attached: function () {
      this._getVideoList(); // 在组件实例进入页面节点树时执行
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
  }
})