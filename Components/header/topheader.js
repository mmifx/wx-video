Component({
  //样式表互不影响
  options: {
    styleIsolation: 'isolated'
  },
  data: {
    NavListName: []
  },
  properties: {
    url: String,
    logosrc: String,
    tips: String
  },
  methods: {
    golaunch() {
      wx.switchTab({
        url: "/pages/index/index"
      })
    },
    _getsearch() {
      wx.navigateTo({
        url: '/pages/search/search'
      })
    },
    _getnavlist() {
      let that = this;
      wx.request({
        url: this.data.url + 'api.php/provide/vod/?ac=list',
        success(res) {
          that.setData({
            NavListName: res.data.class
          })

        }
      })
    }
  },
  lifetimes: {
    attached: function () {
      this._getnavlist(); // 在组件实例进入页面节点树时执行
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
  }
})