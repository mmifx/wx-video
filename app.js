App({
  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {
  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {

  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {

  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {

  },

  globalData: {
    // 这里填maccms网址
    maccmsweb: 'http://v.mmifx.com/',
    gonggao: '晚空，自己写着玩的。乱传播的是王八',
    // 这里是左上角的logo及懒加载 都是用一张图，，
    logosrc: 'https://www.mmifx.com/logo.png',
    // 名字
    name: '茗门影院',
    // 联系方式，暂时没用到
    QQ: '1549072621',
    wechat: 'mmifx666',
    // 播放组 仅支持m3u8及mp4格式
    playerList: [{
      title: "麻花资源",
      name: "mahua"
    }, {
      title: "最大资源",
      name: "zuidam3u8"
    }]
  }
})