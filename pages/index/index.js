var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    maccmsweb: app.globalData.maccmsweb,
    logosrc: app.globalData.logosrc,
    gonggao: app.globalData.gonggao,
    maccmsweb: app.globalData.maccmsweb,
    recommendList: [],
    name: app.globalData.name,
    NowPages: 1,
  },
  toplay: function (e) {
    wx.navigateTo({
      url: '/pages/play/play?id=' + e.currentTarget.dataset.videoid + '&num=' + e.currentTarget.dataset.num,
    })
  },
  getrecommendList() {
    let that = this;
    wx.request({
      url: that.data.maccmsweb + '/api.php/provide/vod/?ac=detail&level=9',
      success(res) {
        that.setData({
            recommendList: res.data.list
          }),
          wx.stopPullDownRefresh()
      }
    })
  },
  GetNextPages(num) {},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getrecommendList();
  },
  // 获取推荐信息

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.setNavigationBarTitle({
      title: this.data.name
    })
    // console.log(app.globalData)
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getrecommendList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})