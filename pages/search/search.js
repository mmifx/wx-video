// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    maccmsweb: getApp().globalData.maccmsweb,
    logosrc: getApp().globalData.logosrc,
    name: getApp().globalData.name,
    gonggao: getApp().globalData.gonggao,
    showDialog: false,
    searchkey: '',
    oneButton: [{
      text: '确定'
    }],
    resultList: null,
    resultShow:false
  },
  GetNowtitle(keyword) {
    let that = this;
    wx.setNavigationBarTitle({
      title: '搜索到《' + keyword + '》的结果' + '——' + that.data.name
    })
  },
  tapDialogButton() {
    let that = this;
    that.setData({
      showDialog: false
    })
  },
  toplay: function(e) {
    wx.navigateTo({
      url: '/pages/play/play?id=' + e.currentTarget.dataset.id + '&num=' + e.currentTarget.dataset.num,
    })
  },
  GetSearchresult(key) {
    let that = this;
    wx.request({
      url: that.data.maccmsweb + 'api.php/provide/vod/?ac=detail&wd=' + key,
      success(res) {
        that.setData({
          resultList: res.data.list,
          resultShow:true
        })
        that.GetNowtitle(key);
      }
    })
  },
  FormSubmit: function (e) {
    let that = this;
    var keyword = e.detail.value.input;
    if (keyword === "") {
      that.setData({
        showDialog: true
      })
    } else {
      that.GetSearchresult(keyword);
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   // this.GetSearchresult("叶问")
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})