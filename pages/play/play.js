// pages/play/play.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    videoId: '35303',
    showTips: false,
    showValue: '123456',
    maccmsweb: app.globalData.maccmsweb,
    logosrc: app.globalData.logosrc,
    name: app.globalData.name,
    gonggao: app.globalData.gonggao,
    playsource: app.globalData.playerList,
    videoDetail: null, //存放最终结果
    videoPlayer: null,
    videoplayfrom: '', //存放播放器组分类
    showControl: true,
    NowNum: 0,
    NowPlayType: 0,
    NowPlayUrl: '', //现在正在播放的视频地址
    lunboTips: ''
  },
  // 视频缓冲完成
  VideoLoadComplete: function (e) {
    let that = this;
    console.log(e);
    wx.getStorage({
      key: that.data.videoId.toString(),
      success: (res) => {
        var resdata = JSON.parse(res.data)
        if (resdata[that.data.NowNum] != null) {
          var videoContext = wx.createVideoContext('video', this);
          videoContext.seek(resdata[that.data.NowNum] * 0.01 * e.detail.duration)
        }
      },
      fail: () => {},
      complete: () => {}
    })
  },
  // 进度发生改变
  VideoProgress: function (e) {
    let that = this;
    // console.log(e);
    var vdata = new Array(that.data.videoPlayer[that.data.NowPlayType].data.length);
    vdata[that.data.NowNum] = e.detail.buffered;
    wx.setStorage({
      key: that.data.videoId.toString(),
      data: JSON.stringify(vdata)
    });
  },
  // 开始播放  时 的事件
  StartPlay: function (e) {
    let that = this;
    console.log(e)
    // 取本地存储

  },
  // 取当前标题 设置到顶部和公告轮播里
  GetNowtitle: function () {
    let that = this;
    that.setData({
      lunboTips: '正在播放《' + this.data.videoDetail.vod_name + '》' + this.data.videoPlayer[0]['data'][that.data.NowNum].name + '————————' + this.data.gonggao
    })
    wx.setNavigationBarTitle({
      title: '正在播放《' + this.data.videoDetail.vod_name + '》' + this.data.videoPlayer[0]['data'][that.data.NowNum].name
    })
  },
  // 取当前食品播放链接组
  GetVideoPlayUrl(videofrom, playurl) {
    let that = this;
    var temp = 0,
      i = 0,
      j = 0;
    var videoTypeName = Array();
    var res = Array(); //分好播放器组后的结果存放
    var overnum = Array(); //分好集数后的结果存放
    var result = Array(); //最终结果
    var videotype = Array(); //播放器分组存放
    // 查找播放器中中为m3u8源的数据
    // console.log(froms);
    var froms = videofrom.split("$$$");
    for (i = 0; i < froms.length; i++) {
      for (j = 0; j < that.data.playsource.length; j++) {
        if (froms[i] == that.data.playsource[j].name) {
          videotype[temp] = i;
          videoTypeName[i] = that.data.playsource[j].title
          temp++;
        }
      }
    }
    // console.log(playurl);
    // 查找结束，结果存放到videotype数组中
    var playurls = playurl.split("$$$");
    //将含有m3u8数据源的播放地址提取出来，并做分集处理，存放到res中
    for (i = 0; i < videotype.length; i++) {
      // 控制播放器组 
      res[i] = Array();
      for (j = 0; j < playurls[i].length; j++) {
        // 拆分播放器集数 
        res[i] = playurls[videotype[i]].split("#");
      }
    }
    //提取完成。
    // console.log(res);
    //将res中的数据把标题和播放地址分离开
    for (i = 0; i < res.length; i++) {
      overnum[i] = Array();
      for (j = 0; j < res[i].length; j++) {
        overnum[i][j] = res[i][j].split("$");
      }
    }
    //console.log(overnum)
    // var obj = {};
    //console.log(overnum);
    // console.log(videoTypeName)
    for (i = 0; i < res.length; i++) {
      result[i] = {}
      result[i].name = videoTypeName[i];
      result[i].data = Array()
      // console.log(result)
      for (j = 0; j < res[0].length; j++) {
        result[i]['data'][j] = {};
        result[i]['data'][j].name = overnum[i][j][0];
        result[i]['data'][j].url = overnum[i][j][1];
      }
    }
    return result;
  },
  // 视频出错提示
  videoError: function (e) {
    console.log(e)
  },
  GetVideovalue: function (id, num) {
    let that = this;
    wx.request({
      url: that.data.maccmsweb + 'api.php/provide/vod/?ac=detail&ids=' + id,
      success(res) {
        that.setData({
          NowNum: num,
          NowPlayType: 0,
          videoDetail: res.data.list[0],
          videoId: res.data.list[0].vod_id,
          videoPlayer: that.GetVideoPlayUrl(res.data.list[0].vod_play_from, res.data.list[0].vod_play_url),
          NowPlayUrl: that.GetVideoPlayUrl(res.data.list[0].vod_play_from, res.data.list[0].vod_play_url)[0]['data'][num].url
        })
        that.GetNowtitle();
        wx.stopPullDownRefresh();
      }
    })
  },
  // 选集
  chosepart: function (url) {
    let that = this;
    that.setData({
      NowPlayUrl: url.currentTarget.dataset.url,
      NowNum: url.currentTarget.dataset.num
    })
    that.GetNowtitle();
  },
  // 上一集
  headone: function () {
    let that = this;
    if (this.data.NowNum == 0) {
      that.setData({
        showTips: true,
        showValue: '已经是第一集了'
      })
      console.log(this.data.NowNum)
    } else {
      that.setData({
        NowNum: this.data.NowNum - 1,
        NowPlayUrl: this.data.videoPlayer[this.data.NowPlayType]['data'][this.data.NowNum]['url']
      })
      that.GetNowtitle();
    }
  },
  // 下一集
  nextone: function () {
    let that = this;
    if (this.data.NowNum == this.data.videoPlayer[this.data.NowPlayType]['data'].length - 1) {
      that.setData({
        showTips: true,
        showValue: '已经是最后一集了'
      })
    } else {
      that.setData({
        NowNum: this.data.NowNum + 1,
        NowPlayUrl: this.data.videoPlayer[that.data.NowPlayType]['data'][that.data.NowNum]['url']
      })
      that.GetNowtitle();
    }
  },
  // 刷新
  shuaxin: function () {
    let that = this;
    wx.startPullDownRefresh({
      complete() {
        that.GetVideoId(that.data.videoId, that.data.NowNum)
      }
    })
  },
  GetVideoId: function (id, num) {
    if (id == null) {
      id = 35303;
    }
    if (num == null) {
      num = 0;
    }
    this.GetVideovalue(id, num);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      videoId: options.id
    })
    this.GetVideoId(options.id, options.num);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    VideoContext.pause()
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})